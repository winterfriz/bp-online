/**
 * Created by sviatoslav on 03.04.18.
 */
const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');

// User Schema
const UserSchema = mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    phone:{
        type: String,
        required: true
    },
    email:{
        type: String,
        required: true
    },
    password:{
        type: String,
        required: true
    },
    BPs: {
        type: Array
    }
});

var User = module.exports = mongoose.model('User', UserSchema);