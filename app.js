"use strict";


// set up  ============================================================================================================
const express = require('express');
const app = express();
const port = 3000;

var mongoose = require('mongoose');
// var passport = require('passport');
var flash    = require('connect-flash');
var path    = require("path");
const bcrypt = require('bcrypt-nodejs');

var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');

const config = require('./config/database');
const User = require('./models/user');

// configuration ======================================================================================================
mongoose.connect(config.database);
let dbm = mongoose.connection;

// Check connection
dbm.once('open', function() {
    console.log('Connected to MongoDB');
});

// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser()); // get information from html forms
app.set('view engine', 'html'); // set up ejs for templating

// required for passport
app.use(flash()); // use connect-flash for flash messages stored in session

app.use(session({ secret: 'ilovemars', cookie: { maxAge: 60000 } })); // session secret

app.get('*', function(req, res, next){
    console.log("app.get('*'");
    res.locals.user = req.user || null;
    next();
});

// app.get('/login', function(req, res) {
//     console.log("app.get('/login'");
//     var file = path.join(__dirname+'/templates/'+'login.html');
//     res.sendFile(file);
// });

app.post(
    '/login',
    function(req, res) {
        console.log("app.post('/login'");
        const email = req.body.email;
        const password = req.body.password;
        console.log(email+" "+password);

        let query = {email:email, password:password};
        User.findOne(
            query,
            function(err, user) {
                if (err) throw err;
                if (user) {
                    console.log("    user find");
                    res.json({user_id: user._id});
                    //res.redirect("/");
                    console.log("    index.html");
                } else {
                    console.log("    user not find'");
                    res.sendStatus(404);
                }
            }
        );
});

// app.get('/register', function(req, res) {
//     console.log("app.get('/register'");
//     var file = path.join(__dirname+'/templates/'+'register.html');
//     res.sendFile(file);
// });

app.post('/register', function(req, res){
    console.log("app.post('/register'");
    const name = req.body.name;
    const phone = req.body.phone;
    const email = req.body.email;
    const password = req.body.password;

    let query = {email:email};
    User.findOne(
        query,
        function(err, user) {
            if (err) throw err;
            if (!user) {
                var newUser = new User({
                    name:name,
                    phone:phone,
                    email:email,
                    password:password
                });

                console.log(newUser);
                newUser.save();
                res.redirect('/');
            } else {
                res.sendStatus(204);
            }
        }
    );
});

app.use('/static', express.static('static'));
app.use('/views', express.static('views'));

var mongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
var db;
mongoClient.connect(url, function(err, database) {
    if (err) throw err;
    db = database.db("BP");
});

app.get('/', (req, res) => {
    console.log("app.get('/', (req, res)");
    var file = path.join(__dirname+'/views/'+'index.html');
    console.log(file);
    res.sendFile(file);
    console.log("    end app.get('/', (req, res)");
});

app.get('/userdata/:user_id', (request, response) => {
    db.collection('users').find({_id: mongoose.Types.ObjectId(request.params.user_id)}).toArray(function (err, docs) {
        if (err) {
                return response.sendStatus(500);
            }
        response.send(docs[0]);
    });
});

app.post('/BP/:user_id/:BP_id', (request, response) => {
    console.log("app.post('/BP/:user_id/:BP_id')");
    const BP_body = request.body;
    var BP_id = parseInt(request.params.BP_id);
    db.collection('users').updateOne(
        {
            _id: mongoose.Types.ObjectId(request.params.user_id),
            BPs: { $elemMatch: { _id: { $eq: BP_id } } }
        },
        { $set: { "BPs.$": BP_body } }
    );
    response.sendStatus(200);
});

app.post('/newBP/:user_id', (request, response) => {
    console.log("app.post('/newBP',...)");
    const body = request.body;
    db.collection('users').update(
        { _id: mongoose.Types.ObjectId(request.params.user_id) },
        { $push:
                {
                    BPs: body
                }
        },
        { new: true },
        function (err, documents) {
            response.send({ error: err, affected: documents });
        }
    );
});

app.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }

  console.log(`server is listening on ${port}`)
});