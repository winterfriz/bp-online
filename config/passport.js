/**
 * Created by sviatoslav on 03.04.18.
 */
const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/user');
const config = require('../config/database');
const bcrypt = require('bcrypt-nodejs');

module.exports = function(passport){
    // Local Strategy
    passport.use(
        new LocalStrategy(
            function(username, password, done){
                // Match Username
                let query = {email:username};
                User.findOne(
                    query,
                    function(err, user) {
                        if (err) throw err;
                        
                        if (!user) {
                            return done(null, false, {message: 'No user found'})
                        }
                        
                        // Match Password
                        if (user.password === password) {
                            return done(null, user);
                        } else {
                            return done(null, false, {message: 'Wrong password'});
                        }
                    }
                )
            }
        )
    );

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });
};